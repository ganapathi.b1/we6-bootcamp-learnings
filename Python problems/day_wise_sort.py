def split_filename(filename: str):
    for i, ch in enumerate(filename):
        if ch.isdigit():
            break
    return filename[:i], int(filename[i:])

def sort_files_by_number(files):
    return sorted(files, key=lambda x: split_filename(x))
print(sort_files_by_number(["day1", "day2", "day23", "day14", "day19"]))

