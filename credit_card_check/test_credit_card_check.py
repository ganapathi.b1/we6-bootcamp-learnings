import unittest
from credit_card_check import double_second_digits, sum_digits, calculate_check_digit

class TestCreditCardCheck(unittest.TestCase):

    def test_double_second_digits(self):
        self.assertEqual(double_second_digits(1789372997), [2, 9, 16, 9, 6, 6, 4, 18, 2, 16])

    def test_sum_digits(self):
        self.assertEqual(sum_digits([2, 9, 16, 9, 6, 6, 4, 18, 2, 16]), 98)

    def test_calculate_check_digit(self):
        self.assertEqual(calculate_check_digit(1789372997), 4)

if __name__ == '__main__':
    unittest.main()
