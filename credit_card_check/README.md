# Credit Card Check Algorithm

## Introduction

The credit card check algorithm is used to verify the validity of a credit card number. It involves the following steps:

1. Start from the rightmost digit of the credit card number.
2. Moving left, double the value of every second digit (including the rightmost digit).
3. Sum the values of the resulting digits.
4. The check digit is the smallest number (possibly zero) that must be added to the sum to make it a multiple of 10.

This README provides a comprehensive guide on the credit card check algorithm, including implementation details, usage instructions, and testing information.

## Algorithm Explanation

The credit card check algorithm works as follows:

1. Iterate through each digit of the credit card number from right to left.
2. Double the value of every second digit.
3. If doubling a digit results in a number greater than 9, subtract 9 from the result.
4. Sum all the resulting digits.
5. Find the check digit by taking the remainder of the sum divided by 10. If the remainder is not zero, subtract it from 10 to get the check digit.

## Implementation

The following Python script implements the credit card check algorithm:

### `credit_card_check.py`

```python
def double_second_digits(card_number: int) -> list[int]:
    return [int(digit) * 2 if index % 2 == 0 else int(digit) for index, digit in enumerate(reversed(str(card_number)))]

def sum_digits(digits: list[int]) -> int:
    return sum([digit - 9 if digit > 9 else digit for digit in digits])

def calculate_check_digit(card_number: int) -> int:
    doubled_digits = double_second_digits(card_number)
    summed_digits = sum_digits(doubled_digits)
    check_digit = (10 - (summed_digits % 10)) % 10
    return check_digit

def main():
    card_number = 1789372997
    check_digit = calculate_check_digit(card_number)
    print(f"Check digit for {card_number}: {check_digit}")

if __name__ == "__main__":
    main()
```

## Usage

1. Ensure you have Python installed on your system.
2. Clone the repository containing the credit card check implementation:

    ```bash
    git clone https://gitlab.com/yourusername/credit_card_check.git
    cd credit-card-check
    ```

3. Run the script to check the validity of a credit card number:

    ```bash
    python credit_card_check.py
    ```

## Testing

To ensure the correctness of the implementation, a test file is included. You can run the tests using `unittest`.

### `test_credit_card_check.py`

```python
import unittest
from credit_card_check import double_second_digits, sum_digits, calculate_check_digit

class TestCreditCardCheck(unittest.TestCase):

    def test_double_second_digits(self):
        self.assertEqual(double_second_digits(1789372997), [2, 9, 16, 9, 6, 6, 4, 18, 2, 16])

    def test_sum_digits(self):
        self.assertEqual(sum_digits([2, 9, 16, 9, 6, 6, 4, 18, 2, 16]), 98)

    def test_calculate_check_digit(self):
        self.assertEqual(calculate_check_digit(1789372997), 4)

if __name__ == '__main__':
    unittest.main()

```

### Running Tests

1. Install `unittest` if you haven't already:

    ```bash
    pip install unittest
    ```

2. Run the tests:

    ```bash
    pytest test_credit_card_check.py
    ```

## Directory Structure

Your project directory should look like this:

```
credit-card-check/
├── credit_card_check.py
├── test_credit_card_check.py
└── LICENSE
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request for any changes or improvements.

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Open a pull request.

## Acknowledgments

- This implementation of the credit card check algorithm is based on standard algorithmic approaches.

Thank you for your interest in this project! If you have any questions, feel free to open an issue or contact the project maintainers.