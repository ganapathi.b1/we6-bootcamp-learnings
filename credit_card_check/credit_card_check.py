def double_second_digits(card_number: int) -> list[int]:
    return [int(digit) * 2 if index % 2 == 0 else int(digit) for index, digit in enumerate(reversed(str(card_number)))]

def sum_digits(digits: list[int]) -> int:
    return sum([digit - 9 if digit > 9 else digit for digit in digits])

def calculate_check_digit(card_number: int) -> int:
    doubled_digits = double_second_digits(card_number)
    summed_digits = sum_digits(doubled_digits)
    check_digit = (10 - (summed_digits % 10)) % 10
    return check_digit

def main():
    card_number = 1789372997
    check_digit = calculate_check_digit(card_number)
    print(f"Check digit for {card_number}: {check_digit}")

if __name__ == "__main__":
    main()
