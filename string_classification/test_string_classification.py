import pytest
from string_classification import classify_string

def test_classify_string():
    assert classify_string("hello") == "Type A"
    assert classify_string("zxp") == "Type D"
    assert classify_string("abczyx") == "Type P"
    assert classify_string("zyxabc") == "Type V"
    assert classify_string("mixedstring") == "Type X"
    assert classify_string("a") == "Type A"  # Single character edge case
    assert classify_string("ab") == "Type A"  # Two characters ascending
    assert classify_string("ba") == "Type D"  # Two characters descending

if __name__ == "__main__":
    pytest.main()