HYPHEN = "-"
def clean(isbn: str) -> list[int]:
    return [int(ch) for ch in isbn if ch != HYPHEN]


def check_isbn(isbn: str) -> bool:
    return check_digit(isbn[:-1]) == int(isbn[-1])

def calculate_check_digit(isbn: str) -> int:
    weights = [1,3] * 6
    check_digits = sum([a * b for (a, b) in zip(weights, clean(isbn))]) % 10
    return 0 if check_digits == 0 else 10 - check_digits

ISBN = "978-3-16-148410-0"
print(f"Is {ISBN} valid? {check_isbn(ISBN)}")
