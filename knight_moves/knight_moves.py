# knight_moves.py

BOARD_SIZE = 8

def is_valid_move(x: int, y: int, visited: list[list[bool]]) -> bool:
    """
    Check if the move (x, y) is valid on the given board size and hasn't been visited yet.
    """
    return 0 <= x < BOARD_SIZE and 0 <= y < BOARD_SIZE and not visited[x][y]

def possible_moves(x: int, y: int, visited: list[list[bool]]) -> list[tuple[int, int]]:
    """
    Generate all possible moves from the current position (x, y) on the board.
    """
    moves = [
        (x + 1, y + 2),
        (x + 1, y - 2),
        (x - 1, y + 2),
        (x - 1, y - 2),
        (x + 2, y + 1),
        (x + 2, y - 1),
        (x - 2, y + 1),
        (x - 2, y - 1)
    ]
    return [(a, b) for a, b in moves if is_valid_move(a, b, visited)]

def min_moves_to_cover_board(START_X: int, START_Y: int) -> int:
    """
    Calculate the minimum number of moves required for the knight to cover all squares on the chess board.
    """
    moves = 0
    visited = [[False for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]
    visited[START_X][START_Y] = True

    while True:
        if all(all(row) for row in visited):
            return moves
        for i in range(BOARD_SIZE):
            for j in range(BOARD_SIZE):
                if visited[i][j]:
                    continue
                next_moves = possible_moves(i, j, visited)
                if next_moves:
                    visited[i][j] = True
        moves += 1
