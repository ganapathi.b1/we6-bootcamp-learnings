# Knight Moves on a Chessboard

## Introduction

The "Knight Moves" problem involves calculating the minimum number of moves required for a knight to cover all squares on a standard 8x8 chessboard. This README provides an overview of the problem, details of the implementation, usage instructions, testing information, directory structure, license, and contribution guidelines.

## Problem Statement

Given a starting position (x, y) on an 8x8 chessboard, the task is to calculate the minimum number of moves required for a knight to cover all squares on the board, visiting each square only once.

### Example

For example, if the starting position is (0, 0), the expected output is `63`, indicating that the knight needs to make 63 moves to cover all squares on the chessboard.

## Implementation

The implementation consists of the following components:

1. `BOARD_SIZE`: Global variable representing the size of the chessboard.
2. `IS_VALID_MOVE`: Function to check if a move (x, y) is valid on the given board size and hasn't been visited yet.
3. `POSSIBLE_MOVES`: Function to generate all possible moves from the current position (x, y) on the board.
4. `MIN_MOVES_TO_COVER_BOARD`: Function to calculate the minimum number of moves required for the knight to cover all squares on the chessboard.

## Usage

To use the Knight Moves algorithm, follow these steps:

1. Clone the repository containing the implementation:

    ```bash
    git clone https://gitlab.com/yourusername/knight_moves.git
    cd knight_moves
    ```

2. Run the script to calculate the minimum number of moves for a given starting position:

    ```bash
    python knight_moves.py
    ```

## Testing

The implementation includes a unittest file (`test_knight_moves.py`) to test the `MIN_MOVES_TO_COVER_BOARD` function. To run the unittests, execute the following command:

```bash
python test_knight_moves.py
```

## Directory Structure

The project directory should look like this:

```
knight_moves/
├── knight_moves.py
├── test_knight_moves.py
└── LICENSE
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request for any changes or improvements.

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Open a pull request.

## Acknowledgments

- This implementation of the Knight Moves algorithm is based on standard algorithmic approaches.

Thank you for your interest in this project! If you have any questions, feel free to open an issue or contact the project maintainers.